package org.example.oauth2_google_calendar_client.controller;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Events;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.GeneralSecurityException;

@RestController
@RequestMapping("/api/calendar")
public class CalenderController {

    @GetMapping
    public Events calendar(@RegisteredOAuth2AuthorizedClient("google")OAuth2AuthorizedClient client) throws IOException, GeneralSecurityException {

        String token = client.getAccessToken().getTokenValue();

        System.out.println(client.getClientRegistration().getScopes());

        GoogleCredential googleCredential = new GoogleCredential().setAccessToken(token);

        NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

        Calendar calendar = new Calendar
                .Builder(httpTransport, new GsonFactory(), googleCredential)
                .build();

        return calendar.events().list("primary").execute();

    }

}
