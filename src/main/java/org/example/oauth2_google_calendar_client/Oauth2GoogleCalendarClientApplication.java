package org.example.oauth2_google_calendar_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Oauth2GoogleCalendarClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(Oauth2GoogleCalendarClientApplication.class, args);
    }

}
